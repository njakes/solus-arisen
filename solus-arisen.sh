#!/usr/bin/env bash
set -e
# Thank you DasGeek and RiderExMachina for the inspiration
# Initial release for Solus
# Version 0.0.1

## Define Temporary Directory Location - "ais" stands for "Auto Install Script"
#tmp_dir=/tmp/ais

## Define some variables to make it less typing
install= eopkg it --yes
#update=eopkg ur && eopkg up --yes
user=$USER

## Check if snapd is installed just in case
check_snap () {
  if ! [ -e /usr/lib/snapd ] ; then
      $install snapd
  fi
}

## Start Script
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root"
  exit 1
else
  #Update and Upgrade
  #echo "Updating and Upgrading"
  #$update

  echo "Creating temporary folder for install files"
#  mkdir $tmp_dir

  $install dialog
  cmd=(dialog --title "Package installer" --separate-output --checklist "Please select the software you want to install:" 22 80 16)
  options=(
    #A "<----Category: Software Repositories---->" on
      1_repos "   Install Flatpak Repository" off
      2_repos "   Install Snaps Repository" off
    #B "<----Category: Notes---->" on

    #C "<----Category: Social---->" on
      1_social "    Telegram (Snap)" off
      2_social "    Telegram (Flatpak)" off
      3_social "    Discord (Snap)" off
      4_social "    Discord (Flatpak)" off
      5_social "    Signal (Snap)" off
      6_social "    Signal (Flatpak)" off
      7_social "    Slack (Snap)" off
    #D "<----Category: Tweaks---->" on
      1_tweaks "    Elementary Tweaks" off
      2_tweaks "    Ubuntu Restricted Extras" off
      3_tweaks "    Gnome Tweak Tool" off
      4_tweaks "    Balena Etcher" off
      5_tweaks "    Deja-Dup" off
    #E "<----Category: Media---->" on
      1_media "     Lollypop" off
      2_media "     VLC" off
      3_media "     Nomacs" off
      4_media "     Simple Screen Recorder" off
      5_media "     YouTube-DL" off
    #F "<----Category: Internet---->" on
      1_internet "      Brave Browser" off
      2_internet "      Vivaldi Browser" off
    #G "<----Category: Video, Audio & Pic Editing---->" on
      1_edit "    GIMP" off
      2_edit "    Inkscape" off
    #H "<----Category: Security---->" on
      1_security "    ProtonVPN CLI" off
      2_security "    Bitwarden" off

    #I "<----Category: Misc---->" on
      1_misc "    Fondo Wallpapers" off
      2_misc "    Variety Wallpaper Utility" off
    #J "<----Category: Coding & FTP---->" on
      1_code "    GitKraken" off
      2_code "    Sublime Text" off
      3_code "    Atom" off
      4_code "    Filezilla" off
    #K "<----Category: Gaming & Fun---->" on
      1_game "    Steam" off
      2_game "    Lutris" off
      3_game "    gamemode" off
    #L "<----Category: Office---->" on
      1_office "    ProtonMail Client (ElectronMail)" off
      2_office "    ProtonMail Client (protonmail-desktop-unofficial)" off
      3_office "    Solus Microsoft Core Fonts" off
      4_office "    O20 Office" off
    M "Post Install Auto Clean Up & Update" off)
    choices=$("${cmd[@]}" 2>&1 >/dev/tty)
    clear
    for choise in $choices
    do
        case $choise in
# Section A --------------Repos--------------------
    1_repos)
      #Install Flatpak Repos
      echo "Installing Flatpak Repository"
      $install flatpak xdg-desktop-portal-gtk -y
      flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
      ;;
    2_repos)
      #Install Snap Repo
      echo "Installing Snap Repository"
      check_snap
      sleep 1
      ;;
# Section B --------------Notes--------------------

# Section C --------------Social--------------------
    1_social)
    echo "Installing Telegram Snap"
    check_snap
    snap install telegram-desktop
    ;;
    2_social)
    echo "Installing Telegram Flatpak"
    flatpak install flathub org.telegram.desktop
    ;;
    3_social)
    echo "Installing Discord Snap"
    check_snap
    snap install discord
    ;;
    4_social)
    echo "Installing Discord Flatpak"
    flatpak install flathub com.discordapp.Discord
    ;;
    5_social)
    echo "Installing Signal Snap"
    check_snap
    snap install signal-desktop
    ;;
    6_social)
    echo "Installing Signal Flatpak"
    flatpak install flathub org.signal.Signal
    ;;
    7_social)
    echo "Installing Slack Snap"
    check_snap
    snap install slack --classic
    ;;
# Section D --------------Tweaks--------------------
    1_tweaks)
    echo "Installing Elementary Tweaks - Elementary OS ONLY"
    $install software-properties-common
    $install elementary-tweaks
    sleep 1
    ;;
    2_tweaks)
    echo "Installing Ubuntu Restricted Extras - Ubuntu Based ONLY"
    $install ubuntu-restricted-extras
    sleep 1
    ;;
    3_tweaks)
    echo "Installing Gnome Tweak Tool"
    $install gnome-tweaks
    ;;
    4_tweaks)
    echo "Installing Balena Etcher"
    $install etcher -y
    ;;
    5_tweaks)
    echo "Installing Deja-Dup"
    $install deja-dup -y
    ;;

# Section E --------------Media--------------------
    1_media)
    echo "Installing Lollypop"
    $install lollypop -y
    ;;
    2_media)
    echo "Installing VLC"
    $install vlc libblueray -y
    ;;
    3_media)
    echo "Installing Nomacs Image Viewer"
    $install nomacs -y
    ;;
    4_media)
    echo "Installing Simple Screen Recorder"
    $install simplescreenrecorder
    ;;
    5_media)
    echo "Installing YouTube-DL"
    $install youtube-dl -y
    ;;
# Section F --------------Internet--------------------
    1_internet)
    echo "Installing Brave Browser"
    $install brave -y
    ;;
    2_internet)
    echo "Installing Vivaldi Browser"
    $install vivaldi-stable -y
    ;;
# Section G --------------Video, Audio & Pic Editing--------------------
    1_edit)
    echo "Installing GIMP"
    $install gimp
    ;;
    2_edit)
    echo "Installing Inkscape"
    $install inkscape
    ;;
# Section H --------------Security--------------------
    1_security)
    echo "Installing ProtonVPN CLI"
    $install protonvpn-cli
    ;;
    2_security)
    echo "Installing Bitwarden"
    flatpak install flathub com.bitwarden.desktop
    ;;
# Section I --------------Misc--------------------
    1_misc)
    echo "Installing Fondo Flatpak"
    flatpak install flathub com.github.calo001.fondo
    ;;
    2_misc)
    echo "Installing Variety"
    $install variety
    ;;
# Section J --------------Coding & FTP--------------------
    1_code)
    echo "Installing GitKraken"
    flatpak install flathub com.axosoft.GitKraken
    ;;
    2_code)
    echo "Installing Sublime Text"
    flatpak install flathub com.sublimetext.three
    ;;
    3_code)
    echo "Installing Atom"
    $install atom
    ;;
    4_code)
    echo "Installing Filezilla"
    $install filezilla -y
    ;;
# Section K --------------Gaming & Fun--------------------
    1_game)
    echo "Installing Steam"
    $install steam
    ;;
    2_game)
    echo "Installing Lutris"
    $install lutris
    ;;
    3_game)
    echo "Installing Gamemode"
    $install gamemode
    ;;
# Section L --------------Office-----------------------
    1_office)
    echo "Installing ProtonMail Client ElectronMail"
    check_snap
    snap install electron-mail
    ;;
    2_office)
    echo "Installing ProtonMail Client (Unofficial)"
    check_snap
    snap install protonmail-desktop-unofficial
    ;;
    3_office)
    echo " Installing Solus Microsoft Core Fonts"
    sudo eopkg bi --ignore-safety https://raw.githubusercontent.com/getsolus/3rd-party/master/desktop/font/mscorefonts/pspec.xml --yes
    sudo eopkg it mscorefonts*.eopkg;sudo rm mscorefonts*.eopkg --yes
    ;;
    4_office)
    echo "Installing 020 Office"
    flatpak install flathub io.gitlab.o20.word
    ;;
  M)
    #Clean up
    echo "Cleaning Up"
    $update
    eopkg rmo
    rm -rf $tmp_dir
    ;;
esac
done
fi
